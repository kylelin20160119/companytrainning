package com.example.demo.dto;

public class CustomerRequestBody {

	public String customerId;

	public String name;

	public String addr;

	public String age;

	public String tel;

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	@Override
	public String toString() {
		return "CustomerRequestBody [customerId=" + customerId + ", name=" + name + ", addr=" + addr + ", age=" + age
				+ ", tel=" + tel + "]";
	}

}

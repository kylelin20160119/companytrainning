package com.example.homeDemo.repositories;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.homeDemo.entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer, String> {

	@Query(value ="select CUSTOMER_ID, NAME, ADDR, AGE, TEL " +
			"from CUSTOMER_NEW where " + 
			"CUSTOMER_ID = (case when :customerId = '' then CUSTOMER_ID else :customerId end)"+ 
			"and (NAME = (case when :name = '' then NAME else :name end))" +
			"and (ADDR = (case when :addr = '' then ADDR else :addr end))" +
			"and (AGE = (case when :age = '' then AGE else :age end))" +
			"and (TEL = (case when :tel = '' then TEL else :tel end))"
			, nativeQuery = true)
	List<Customer> findBySpecificColumn(
			@Param("customerId") String customerId 
			, @Param("name") String name
			, @Param("addr") String addr
			, @Param("age") String age
			, @Param("tel") String tel
			);
/*
	List<Customer> findByLastName(String lastName);

	List<Customer> findByFirstNameContaining(String bauer);

	List<Customer> findByFirstName(String bauer);

	@Query("select c from Customer c where c.firstName= ?1")
	List<Customer> findByFirstName2(String firstName);

	@Query("select c from Customer c where c.firstName= :firstName")
	List<Customer> findByFirstName3(@Param("firstName") String firstName);
	
	@Query("select c from Customer c where c.id=:id and c.firstName= :firstName")
	List<Customer> findByMultipleCol(@Param("id") Long id
			,@Param("firstName") String firstName);

*/
}
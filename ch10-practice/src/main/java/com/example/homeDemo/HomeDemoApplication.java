package com.example.homeDemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HomeDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(HomeDemoApplication.class, args);
	}

}

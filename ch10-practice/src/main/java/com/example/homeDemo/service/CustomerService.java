package com.example.homeDemo.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.example.demo.dto.CustomerRequest;
import com.example.demo.dto.CustomerResponse;
import com.example.homeDemo.entity.Customer;
import com.example.homeDemo.repositories.CustomerRepository;


public interface CustomerService {

	void save(Customer c);

	List<Customer> findAll();

	List<Customer> findBySpecificColumn(Customer c);

	CustomerResponse queryDataByPost(String url, CustomerRequest request, Class<CustomerResponse> class1);

	CustomerResponse queryDataByGet(String string, Class<CustomerResponse> class1);
	
	ResponseEntity<CustomerResponse> queryDataByPostByEntity(String url, Map<String,String> request, Class<CustomerResponse> class1);

	CustomerResponse queryDataByPost2(String url, Map<String,String> request, Class<CustomerResponse> class1);
	

	
	

}

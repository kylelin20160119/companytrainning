package com.example.homeDemo.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.example.demo.dto.CustomerRequest;
import com.example.demo.dto.CustomerResponse;
import com.example.homeDemo.entity.Customer;
import com.example.homeDemo.repositories.CustomerRepository;
import com.example.homeDemo.service.CustomerService;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private CustomerRepository customerRepository;

	@Override
	public void save(Customer c) {
		customerRepository.save(c);

	}

	@Override
	public List<Customer> findAll() {
		return customerRepository.findAll();
	}

	@Override
	public List<Customer> findBySpecificColumn(Customer c) {
		return customerRepository.findBySpecificColumn(c.getCustomerId(), c.getName(), c.getAddr(), c.getAge(),
				c.getTel());
	}

	/*
	 * @Override public List<Customer> findBySpecificColumnOne(Customer c) { return
	 * customerRepository.findBySpecificColumnOne( c.getCustomerId(),
	 * c.getName(),c.getAddr(),c.get ); }
	 * 
	 */
	/*
	 * @Bean public RestTemplate getRestTemplate() { return new RestTemplate(); }
	 */
	@Override
	public CustomerResponse queryDataByPost(String url, CustomerRequest request, Class<CustomerResponse> responseType) {
		return new RestTemplate().postForObject(url, request, responseType);

	}

	@Override
	public CustomerResponse queryDataByGet(String url, Class<CustomerResponse> responseType) {
		return new RestTemplate().getForObject(url, responseType);
	}

	@Override
	public ResponseEntity<CustomerResponse> queryDataByPostByEntity(String url, Map<String,String> request,
			Class<CustomerResponse> responseType) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		MultiValueMap<String, String> params= new LinkedMultiValueMap<String, String>();

		params.add("ageUp", request.get("ageUp"));
		params.add("ageDown", request.get("ageDown"));
		HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<MultiValueMap<String, String>>(params,headers);
		return new RestTemplate().postForEntity(url, requestEntity, responseType);
	}

	@Override
	public CustomerResponse queryDataByPost2(String url, Map<String, String> request, Class<CustomerResponse> responseType) {
		return new RestTemplate().postForObject(url, request, responseType);
	}

}

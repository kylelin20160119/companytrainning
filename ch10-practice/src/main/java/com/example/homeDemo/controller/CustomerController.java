package com.example.homeDemo.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.dto.CustomerRequest;
import com.example.demo.dto.CustomerRequestBody;
import com.example.demo.dto.CustomerResponse;
import com.example.homeDemo.entity.Customer;
import com.example.homeDemo.service.CustomerService;

@Controller
@RequestMapping("/customer")
public class CustomerController {

	@Value("${api.Url}")
	private String apiUrl;

	@Value("${api.Url.Save}")
	private String apiUrlSave;

	@Value("${api.Url.FindAll}")
	private String findAll;

	@Value("${api.Url.FindBySpecificColumn}")
	private String findBySpecificColumn;
	
	@Value("${api.Url.FindByAge}")
	private String findByAge;
	
	@Value("${api.Url.FindByAddr}")
	private String findByAddr;

	@Autowired
	private CustomerService customerService;

	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String home() {

		return "home";

	}

	@RequestMapping(value = "/select", method = RequestMethod.GET)
	public String select() {

		return "select";

	}

	@RequestMapping(value = "/insert", method = RequestMethod.GET)
	public String insert() {

		return "insert";
	}

	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public String delete() {

		return "delete";

	}

	@RequestMapping(value = "/findAll", method = RequestMethod.GET)
	public String findAll(Model model) {
		//queryDataByGet
		CustomerResponse reponse = customerService.queryDataByGet(
				new StringBuilder().append(apiUrl).append(findAll)
				.toString(), CustomerResponse.class);
		List<Customer> dataList = reponse.getBody().getDataList();
		model.addAttribute("dataList", dataList);
		model.addAttribute("h1","Hi spring data jpa select");
		return "selectGrid";
	}

	@RequestMapping(value = "/findBySpecificColumn", method = RequestMethod.POST)
	public String findBySpecificColumn(@RequestBody String customerData
			,Model model)
			throws UnsupportedEncodingException {
		
		Map<String,String> dataMap = this.stringToMap(customerData);
		CustomerRequestBody c = new CustomerRequestBody();
		c.setCustomerId(dataMap.get("customerId"));
		c.setName(dataMap.get("name"));
		c.setAddr(dataMap.get("addr"));
		c.setAge(dataMap.get("age"));
		c.setTel(dataMap.get("tel"));
		CustomerRequest request = new CustomerRequest();
		request.setBody(c);

		CustomerResponse reponse = customerService.queryDataByPost(
				new StringBuilder(apiUrl).append(findBySpecificColumn).toString()
				, request, CustomerResponse.class);
		
		List<Customer> dataList = reponse.getBody().getDataList();		
		model.addAttribute("dataList",dataList);
		return "selectGrid";
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST
			, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public CustomerResponse save(@RequestBody Map<String,String> dataMap) {
		String customerId = dataMap.get("customerId");
		String name = dataMap.get("name");
		String addr = dataMap.get("addr");
		String age = dataMap.get("age");
		String tel = dataMap.get("tel");
		
		CustomerRequest request = new CustomerRequest(); 
		CustomerRequestBody body = new CustomerRequestBody();
		body.setCustomerId(customerId);
		body.setName(name);
		body.setAddr(addr);
		body.setAge(age);
		body.setTel(tel);
		request.setBody(body);
		
		CustomerResponse reponse = customerService.queryDataByPost(
				new StringBuilder().append(apiUrl).append(apiUrlSave)
				.toString(), request, CustomerResponse.class);

		return reponse;
	}
	
	
	@RequestMapping(value = "/findByAge", method = RequestMethod.POST)
	public String findByAge(@RequestBody String customerData, Model model)
			throws UnsupportedEncodingException {
		Map<String,String> dataMap = this.stringToMap(customerData);
		CustomerResponse reponse = customerService.queryDataByPost2(
				new StringBuilder().append(apiUrl).append(findByAge)
				.toString(), dataMap, CustomerResponse.class);
		
		List<Customer> dataList = reponse.getBody().getDataList();
		model.addAttribute("dataList",dataList);
				
		return "selectGrid";
	}
	
	
	@RequestMapping(value = "/findByAddr", method = RequestMethod.POST)
	public String findByAddr(@RequestBody String customerData
			,Model model)
			throws UnsupportedEncodingException {
		Map<String,String> dataMap = this.stringToMap(customerData);
		CustomerResponse reponse = customerService.queryDataByPost2(
				new StringBuilder().append(apiUrl).append(findByAddr)
				.toString(), dataMap, CustomerResponse.class);
		
		List<Customer> dataList = reponse.getBody().getDataList();
		model.addAttribute("dataList",dataList);
				
		return "selectGrid";
	}

	public Map<String, String> stringToMap(String data) throws UnsupportedEncodingException {
		Map<String, String> queryPairs = new LinkedHashMap<String, String>();

		String[] pairs = data.split("&");
		for (String pair : pairs) {
			int idx = pair.indexOf("=");
			queryPairs.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"),
					URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
		}
		return queryPairs;
	}
	
	@RequestMapping(value = "/findByAgeByResponseEntity", method = RequestMethod.POST)
	public String findByAgeByResponseEntity(@RequestBody String customerData
			,Model model)
			throws UnsupportedEncodingException {
		
		Map<String,String> dataMap = this.stringToMap(customerData);
		ResponseEntity<CustomerResponse> reponse = customerService.queryDataByPostByEntity(
				new StringBuilder(apiUrl).append(findByAge).toString()
				, dataMap, CustomerResponse.class);
		
		List<Customer> dataList = reponse.getBody().getBody().getDataList();
		model.addAttribute("dataList",dataList);

		return "selectGrid";
	}

}

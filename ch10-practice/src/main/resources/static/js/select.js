$(function() {
	//$('#abc').click(function(){ alert('abc')});
	var localUrl= "http://localhost:8080";
    $('button[type=submit]').click(function(){
		btnId = $(this).attr('id');
		textVal = $('#'+btnId).siblings("input").val();
		//alert(btnId);
		//alert(textVal);
			if (btnId == 'findAll') {
				findAllUrl="/customer/findAll";
				findAllData = "";
				type="GET";
				ajaxToSpring(type,findAllUrl,findAllData,"text");
			}else if (btnId == 'selectById'){
				findByIdUrl="/customer/findById";
				findByIdData = {'id':textVal}
				type="POST";
				ajaxToSpring(type,findByIdUrl,findByIdData,"text");
			}else if (btnId == 'selectByFirstName'){
				findByFirstNameUrl="/customer/findByFirstName";
				findByFirstNameData = {'firstName':textVal}
				type="POST";
				ajaxToSpring(type,findByFirstNameUrl,findByFirstNameData,"text");
			}else if (btnId == 'selectByMultipleColumn'){
				findByMultipleColumnData ={};
				columnNum =textVal.split(',').length;
				columnList =textVal.split(',');
				columnList.forEach(function(element) {
					key = element.split(':')[0];
					val = element.split(':')[1];
					findByMultipleColumnData[key]=val;
					
				});
				//alert(JSON.stringify(findByMultipleColumnData));
				findByMultipleColumnUrl="/customer/findByMultipleCol";
				type="POST";
				ajaxToSpring(type,findByMultipleColumnUrl,findByMultipleColumnData,"text");
			}else if (btnId == 'cleanConsole'){
				$('textarea').text('');
			}

	function ajaxToSpring(type,url,data,dataType){
		
		$.ajax({
			type: type,
			url:  url,
			data: data,
			//data: {lastname: select_value},
			dataType : dataType,
			//dataType : "json",
			success: function(data) {
				$("#showArea").text(data);
			},
			error: function(msg) {
				alert("error: " + msg);
			},
			complete: function(jqXHR) {
				alert("complete: " + jqXHR.status + " / " + jqXHR.statusText + " / " + jqXHR.responseText);
			}	
			});
	}

	});	
});


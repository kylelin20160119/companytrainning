package com.example.demo.respository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Customer;

@Repository
public interface CustomerRespository extends JpaRepository<Customer, Integer> {

	@Query(value ="select CUSTOMER_ID, NAME, ADDR, AGE, TEL " +
			"from CUSTOMER_NEW where " + 
			"CUSTOMER_ID = (case when :customerId = '' then CUSTOMER_ID else :customerId end)"+ 
			"and (NAME = (case when :name = '' then NAME else :name end))" +
			"and (ADDR = (case when :addr = '' then ADDR else :addr end))" +
			"and (AGE = (case when :age = '' then AGE else :age end))" +
			"and (TEL = (case when :tel = '' then TEL else :tel end))"
			, nativeQuery = true)
	List<Customer> findBySpecificColumn(
			@Param("customerId") String customerId 
			, @Param("name") String name
			, @Param("addr") String addr
			, @Param("age") String age
			, @Param("tel") String tel
			);

	@Query(value= "select customer_ID,NAME,ADDR,AGE,TEL"
			+ " from CUSTOMER_NEW where AGE >= :ageUp and AGE <= :ageDown"
			,nativeQuery=true)
	List<Customer> findByAge(
			@Param("ageUp")Integer ageUp
			, @Param("ageDown")Integer ageDown);



}

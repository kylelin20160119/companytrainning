package com.example.demo.service;

import java.util.List;
import java.util.Map;

import com.example.demo.entity.Customer;

public interface CustomerService {

	Customer findByCustomerId(Integer customerId);

	void save(Customer costomer);
	
	void delete (Integer customerId);

	List<Customer> findAll();
	
	List<Customer> findBySpecificColumn(Customer costomer);

	List<Customer> findByAge(Map<String,String> map);


}

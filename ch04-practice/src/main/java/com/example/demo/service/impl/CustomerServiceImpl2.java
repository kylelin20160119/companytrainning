package com.example.demo.service.impl;

import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import com.example.demo.entity.Customer;
import com.example.demo.service.CustomerService2;

@Service
public class CustomerServiceImpl2 implements CustomerService2 {
	
	private static final Logger logger = LogManager.getLogger(CustomerServiceImpl2.class);
	
	@Autowired
	@Qualifier(value = "h2jdbcTemplate")
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	/*
	@Autowired
	public CustomerServiceImpl2(@Qualifier("h2DataSource")DataSource dataSource) {
	        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}
	*/

	
	@Override
	public List<Customer> findByAddr(Map<String, String> request) {
		String sql = "SELECT * FROM CUSTOMER_NEW WHERE ADDR = :addr";
		System.out.println("printOut sql" + sql);
		logger.debug("testtest");
		showLogForSql(sql, request);
		return namedParameterJdbcTemplate.query(sql, request
						, new BeanPropertyRowMapper<Customer>(Customer.class));

	}
	/*

	@Override
	public Boolean add(Customer2 user) {
		String sql = "INSERT INTO AUTH_USER(UUID,NAME) VALUES(?,?)";
		return namedParameterJdbcTemplate.update(sql, user.getCustomerId(), user.getName()) > 0;
	}

	@Override
	public Boolean update(Customer2 user) {
		String sql = "UPDATE AUTH_USER SET NAME = ? WHERE UUID = ?";
		return jdbcTemplate.update(sql, user.getName(), user.getCustomerId()) > 0;
	}

	@Override
	public boolean delete(Long id) {
		String sql = "DELETE FROM AUTH_USER WHERE UUID = ?";
		return jdbcTemplate.update(sql, id) > 0;

	}

	@Override
	public Customer2 locate(Long id) {
		String sql = "SELECT * FROM AUTH_USER WHERE UUID=?";
		SqlRowSet rs = jdbcTemplate.queryForRowSet(sql, id);

		if (rs.next()) {
			return generateEntity(rs);
		}
		return null;
	}
	


	@Override
    public List<Customer2> matchName(String name) {
        String sql = "SELECT * FROM AUTH_USER WHERE NAME LIKE ?";
        SqlRowSet rs = jdbcTemplate.queryForRowSet(sql, "%" + name + "%");
        List<Customer2> users = new ArrayList<>();
        while (rs.next()) {
            users.add(generateEntity(rs));
        }
        return users;
    }
	
	
    private Customer2 generateEntity(SqlRowSet rs) {
    	Customer2 weChatPay = new Customer2();
        weChatPay.setCustomerId(rs.getString("UUID"));
        weChatPay.setName(rs.getString("NAME"));
        return weChatPay;
    }
    */
	
    private void showLogForSql(String sql, Map<String, ?> parameter) {

    	logger.debug("SQL : " + sql);
        if (parameter == null || parameter.isEmpty()) {
        	logger.debug("With No Parameter");
        } else {
            for (String key : parameter.keySet()) {
            	logger.debug("Params : {}, Values : {}", key, parameter.get(key));
            }
        }

    }
}


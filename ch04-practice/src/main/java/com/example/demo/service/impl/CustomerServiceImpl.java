package com.example.demo.service.impl;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Customer;
import com.example.demo.respository.CustomerRespository;
import com.example.demo.service.CustomerService;


@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	public CustomerRespository customerRespository;

	@Override
	public Customer findByCustomerId(Integer customerId) {
		Optional<Customer> customerOpt = customerRespository.findById(customerId);
		return customerOpt.orElse(new Customer());
	}

	@Override
	public void save(Customer costomer) {
		
		customerRespository.save(costomer);
		
	}

	@Override
	public void delete(Integer customerId) {
		customerRespository.deleteById(customerId);
		
	}

	@Override
	public List<Customer> findBySpecificColumn(Customer costomer) {

		return customerRespository.findBySpecificColumn(
				costomer.getCustomerId(), costomer.getName(), costomer.getAddr()
				, costomer.getAge(),costomer.getTel());
	}

	@Override
	public List<Customer> findAll() {
		return customerRespository.findAll();
	}

	@Override
	public List<Customer> findByAge(Map<String, String> map) {
		return customerRespository.findByAge(
				Integer.parseInt(map.get("ageUp"))
				,Integer.parseInt(map.get("ageDown")));
	}


}

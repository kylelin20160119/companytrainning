package com.example.demo.service;

import java.util.List;
import java.util.Map;

import com.example.demo.entity.Customer;
import com.example.demo.entity.Customer2;

public interface CustomerService2 {
	
		List<Customer> findByAddr(Map<String, String> request);

	    /**
	     * 向数据库中保存一个新用户
	     *
	     * @param user 要保存的用户对象
	     * @return 是否增肌成功
	     */
	    //Boolean add(Customer2 user);

	    /**
	     * 更新数据库中的一个用户
	     *
	     * @param user 要更新的用户对象
	     * @return 是否更新成功
	     */
	    //Boolean update(Customer2 user);

	    /**
	     * 删除一个指定的用户
	     *
	     * @param id 要删除的用户的标识
	     * @return 是否删除成功
	     */
	    //boolean delete(Long id);

	    /**
	     * 精确查询一个指定的用户
	     *
	     * @param id 要查询的用户的标识
	     * @return 如果能够查询到，返回用户信息，否则返回 null
	     */
	    //Customer2 locate(Long id);

	    /**
	     * 通过名称模糊查询用户
	     *
	     * @param name 要模糊查询的名称
	     * @return 查询到的用户列表
	     */
	    //List<Customer2> matchName(String name);
	    
		

}

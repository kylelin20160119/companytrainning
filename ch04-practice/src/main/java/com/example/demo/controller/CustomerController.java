package com.example.demo.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.CustomerRequest;
import com.example.demo.dto.CustomerResponse;
import com.example.demo.dto.CustomerResponseBody;
import com.example.demo.entity.Customer;
import com.example.demo.service.CustomerService;
import com.example.demo.service.CustomerService2;

@RestController
@RequestMapping("/customer")
public class CustomerController {

	@Autowired
	public CustomerService customerService;
	
	@Autowired
	public CustomerService2 customerService2;

	@PostMapping(value = "/save", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public CustomerResponse save(@RequestBody CustomerRequest request) {
		System.out.println(request);

		CustomerResponse response = new CustomerResponse();
		Customer costomer = new Customer();
		BeanUtils.copyProperties(request.getBody(), costomer);
		customerService.save(costomer);
		CustomerResponseBody body = new CustomerResponseBody();
		body.setReturnCode("0000");
		response.setBody(body);

		return response;
	}

	@GetMapping(value = "/findAll", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public CustomerResponse findAll() {

		CustomerResponse response = new CustomerResponse();
		List<Customer> dataList = customerService.findAll();
		CustomerResponseBody body = new CustomerResponseBody();
		body.setReturnCode("0000");
		body.setDataList(dataList);
		response.setBody(body);

		return response;
	}

	@RequestMapping(value = "/findBySpecificColumn", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public CustomerResponse findBySpecificColumn(@RequestBody CustomerRequest request) {

		CustomerResponse response = new CustomerResponse();
		Customer customer = new Customer();
		BeanUtils.copyProperties(request.getBody(), customer);

		List<Customer> dataList = customerService.findBySpecificColumn(customer);
		CustomerResponseBody body = new CustomerResponseBody();
		body.setReturnCode("0000");
		body.setDataList(dataList);
		response.setBody(body);

		return response;

	}

	@RequestMapping(value = "/findByAge", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public CustomerResponse findByAge(@RequestBody Map<String, String> request) {
		
		CustomerResponse response = new CustomerResponse();

		List<Customer> dataList = customerService.findByAge(request);
		CustomerResponseBody body = new CustomerResponseBody();
		body.setReturnCode("0000");
		body.setDataList(dataList);
		response.setBody(body);

		return response;

	}
	
	@RequestMapping(value = "/findByAddr", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public CustomerResponse findByAddr(@RequestBody Map<String, String> request) {

		CustomerResponse response = new CustomerResponse();

		List<Customer> dataList = customerService2.findByAddr(request);
		CustomerResponseBody body = new CustomerResponseBody();
		body.setReturnCode("0000");
		body.setDataList(dataList);
		response.setBody(body);

		return response;

	}

}

package com.example.demo.config;

import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@Configuration
public class DataSourceConfig {

	private static final Logger logger = LogManager.getLogger(DataSourceConfig.class);

	@Autowired
	private Environment env;

	@Bean(name = "h2DataSource")
	@Qualifier("h2ataSource")
	@Primary
	//@ConfigurationProperties(prefix = "spring.datasource.sqlserver-CTF")
	public DataSource h2ataSource() {
		logger.debug("### h2ataSource");

		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(env.getProperty("spring.datasource.driver-class-name"));
		dataSource.setUrl(env.getProperty("spring.datasource.url"));
		dataSource.setUsername(env.getProperty("spring.datasource.username"));
		dataSource.setPassword(env.getProperty("spring.datasource.password"));

		return dataSource;
	}

	@Bean(name = "h2jdbcTemplate")
	public NamedParameterJdbcTemplate h2jdbcTemplate(@Qualifier("h2DataSource") DataSource dataSource) {
		return new NamedParameterJdbcTemplate(dataSource);
	}

}